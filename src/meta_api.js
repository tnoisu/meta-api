class MetaAPI {
  constructor(params, url) {
    this.params = params;
    this.url_ = url;
  }

  static create(params, url = '') {
    const handler = {
      get: (target, prop) => {
        if (target[prop] === undefined) {
          return target.custom_resource(prop);
        }

        return target[prop];
      }
    };

    const api = new MetaAPI(params, url);
    return new Proxy(api, handler);
  }

  custom_resource(name) {
    let resource = name;

    const escaped = name.match(/_(\d.*)/);
    if (escaped !== null) {
      resource = escaped[1];
    }

    return MetaAPI.create(
      this.params, this.url_ + '/' + resource);
  }

  url(additional_params = {}) {
    const {server} = this.params;
    const params = Object.assign({}, this.params.params, additional_params);

    const get_params = Object.keys(params).reduce((acc, key) => {
      if (acc.length === 0) {
        return `${key}=${params[key]}`;
      } else {
        return `${acc}&${key}=${params[key]}`;
      }
    }, "");

    return encodeURI(`${server}${this.url_}?${get_params}`);
  }
}

module.exports = MetaAPI
