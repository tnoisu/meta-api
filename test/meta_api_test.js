const test = require('ava');
const Mock = require('mock/ava');
const MetaAPI = require('../src/meta_api');

const params = {
  params: {
    apikey: "api_key",
  },
  server: "server.com"
}

test('default url', t => {
  const acc = MetaAPI.create(params);
  t.is(acc.url(), "server.com?apikey=api_key");
});

test('url are properly encoded', t => {
  const acc = MetaAPI.create(params);
  t.is(acc.url({q: '"Moscow"'}), 'server.com?apikey=api_key&q=%22Moscow%22');
});

test('multiple params', t => {
  const params = {
    server: "server.com",
    params: {
      apikey: "api_key",
      language: "ru"
    }
  };

  const acc = MetaAPI.create(params);
  t.is(acc.url(), "server.com?apikey=api_key&language=ru");
});

test('creates complicated urls', t => {
  const acc = MetaAPI.create(params).forecasts.v1.hourly;
  t.is(acc.url(), "server.com/forecasts/v1/hourly?apikey=api_key");
});

test('resources that start with digits are escaped with underscore', t => {
  const acc = MetaAPI.create(params)._223;
  t.is(acc.url(), "server.com/223?apikey=api_key");
});
